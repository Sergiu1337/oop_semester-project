# Short Description

This is the semester project for Object Oriented Programming.

It creates a repository that holds products from a bakery and manipulates such products.

Created by Sergiu Darie, student from 811/2 MIE UBB-CLUJ

## Usage

Console based application with a CUI.

## w.I.P.

    -   Doxygen must be done
    -   This readme must be done better
    -   Undo/Redo must be done
    -   Reading from file is w.i.p.
    -   No tests done yet (test coverage is 0%)
