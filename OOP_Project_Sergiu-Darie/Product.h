#pragma once
#include<iostream>
#include<string>
using namespace std;

class Product {
public:
	Product(int id, double price);

	int GetID() const;
	void SetID(int ID);

	virtual double GetPrice() const;
	void SetPrice(double price);

	virtual ostream& display(ostream& os) const;

	friend ostream& operator <<(ostream& os, const Product& p);

	virtual string getData() const;
private:
	int m_InternalID;
	double m_price;
};
