#include "Bread.h"

string BreadTypes[] = {
	"Sweet Bread",
	"Olives Bread",
	"Garlic Bread",
	"White Bread",
	"Dark Bread",
	"Potato Bread"
};

Bread::Bread(int id, double price, double weight, BreadType type) : Product(id, price)
{
	m_weight = weight;
	m_type = type;
}

double Bread::GetPrice() const
{
	double price = Product::GetPrice() * GetWeight();
	return price;
}

double Bread::GetWeight() const
{
	return m_weight;
}

void Bread::SetWeight(double weight)
{
	m_weight = weight;
}

BreadType Bread::GetType() const
{
	return m_type;
}

string Bread::PrintType() const
{
	return BreadTypes[GetType()];
}

void Bread::SetType(BreadType type)
{
	m_type = type;
}

ostream& Bread::display(ostream& os) const
{
	os << PrintType() << " which weights: " << GetWeight() << "kgs and costs: " << GetPrice() << "ron. ID: " << GetID() << endl;
	return os;
}

string Bread::getData() const
{
	string line;
	line = "Bread, " + to_string(GetID()) + ", " + to_string(GetPrice()) + ", " + to_string(GetWeight()) + ", " + to_string(GetType()) + ", " + PrintType();
	// line += "\n";
	return line;
}

ostream& operator<<(ostream& os, const Bread& p)
{
	return p.display(os);
}
