#pragma once
#include <vector>
#include "Product.h"
#include "Bread.h"
#include "Drink.h"
#include "Pastry.h"
using namespace std;

class ProductRepository
{
private:
	vector<Product*> m_repo;
public:
	bool checkID(int id);

	void addProduct(Product* p);
	void addProduct(Bread* b);
	void addProduct(Pastry* p);
	void addProduct(Drink* d);
	Product* removeProduct(int id);
	ostream& display(ostream& os, bool (*filterFunction)(Product* p));

	string returnString()const;
	void getData(ProductRepository*);
};

// ** 
//CLASA filter ( base )
// subclase filer ( by act , by .... ) 

