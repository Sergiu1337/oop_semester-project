#pragma once
#include"Product.h"

typedef enum
{
	Fuzzy_Drink,
	Calm_Drink,
	Natural_Water,
	Carbonated_Water
}DrinkType;

class Drink : public Product {
public:
	Drink(int id, double price, double volume, DrinkType type, string name);
	/*
	* Default constructor for the Drink object
	Input:
		- int id			: The internal ID of the item
		- double price		: The price of the Drink which is going to be multiplied by the volume
		- double volume		: The volume of the drink in liters
		- DrinkType type	: The type of the drink , I.E. : Fuzzy_Drink, Calm_Drink, Natural_Water, Carbonated_Water
		- string name		: The name of the certain drink
	*/

	double GetPrice() const override;
	/*
	Returns the price of the drink taking in consideration the volume
	Output:
		- returns the price adjusted by the volume of the drink
	*/

	double GetVolume() const;
	/*
	Returns the volume of the drink which is in liters
	Output:
		- returns the volume of the drink in liters
	*/
	void SetVolume(double volume);
	/*
	Changes the volume of the drink which is in liters
	Input:
		- volume			: The volume of the drink
	*/

	DrinkType GetType() const;
	/*
	Returns an integer value of the enum DrinkType
	Output:
		- returns the type which by default is an integer between 0 and 3 which corresponds to the type of the drink
	*/
	string PrintType() const;
	/*
	Returns a string version (corresponding to the enum) of the Type
	Output:
		- returns a string which holds the type of the drink
	*/
	void SetType(DrinkType type);
	/*
	Changes the type of the drink
	Input:
		- DrinkType type	: The type of the drink which is an enumerator (Fuzzy_Drink, Calm_Drink, Natural_Water, Carbonated_Water)
	*/

	string GetName() const;
	/*
	Returns the name of the drink
	Output:
		- returns a string which corresponds to the name of the drink
	*/
	void SetName(string name);
	/*
	Changes the name of the drink
	Input:
		- string name		: The name of the drink
	*/

	ostream& display(ostream& os) const;

	friend ostream& operator <<(ostream& os, const Drink& p);
	/*
	Overload for the << operator which displays info about the drink
	*/

	string getData() const;
private:
	double m_volume;
	DrinkType m_type;
	string m_name;
};
