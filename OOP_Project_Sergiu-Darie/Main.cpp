#include"ProductRepository.h"
#include"ProductController.h"
#include"UI.h"

#include<iostream>
using namespace std;

int main() {
	/*
	Product new_product(123, 5);

	cout << new_product;

	new_product.SetID(321);
	new_product.SetPrice(1);

	cout << new_product;

	new_product.display(cout);

	Drink pepsi1(1, 4, 2, Fuzzy_Drink, "Pepsi");
	cout << pepsi1;

	Pastry muffin(2, 10, 1, Muffin, 2);
	cout << muffin;

	Bread garlicBread(3, 5, 2, Garlic_Bread);
	cout << garlicBread;
	*/

	ProductRepository repo;
	ProductController controller(repo);
	UI ui(controller);

	ui.show();


	return 0;
}
