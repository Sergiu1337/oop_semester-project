#include "Product.h"

Product::Product(int id, double price)
{
	m_InternalID = id;
	m_price = price;
}

int Product::GetID() const
{
	return m_InternalID;
}

void Product::SetID(int ID)
{
	m_InternalID = ID;
}

double Product::GetPrice() const
{
	return m_price;
}

void Product::SetPrice(double price)
{
	m_price = price;
}

ostream& Product::display(ostream& os) const
{
	os << "Product with a price of: " << GetPrice() << " ID: " << GetID() << endl;
	return os;
}

string Product::getData() const
{
	string line;
	line = "Product, " + to_string(GetID()) + ", " + to_string(GetPrice());
	// line += "\n";
	return line;
}

ostream& operator<<(ostream& os, const Product& p)
{
	return p.display(os);
}
