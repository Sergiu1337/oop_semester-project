#include "Drink.h"

string DrinkTypes[] = {
	"Fuzzy Drink",
	"Calm Drink",
	"Natural Water",
	"Carbonated Water"
};

Drink::Drink(int id, double price, double volume, DrinkType type, string name) : Product(id, price)
{
	m_volume = volume;
	m_type = type;
	m_name = name;
}

double Drink::GetPrice() const
{
	double price = Product::GetPrice() * m_volume;
	return price;
}

double Drink::GetVolume() const
{
	return m_volume;
}

void Drink::SetVolume(double volume)
{
	m_volume = volume;
}

DrinkType Drink::GetType() const
{
	return m_type;
}

string Drink::PrintType() const
{
	return DrinkTypes[GetType()];
}

void Drink::SetType(DrinkType type)
{
	m_type = type;
}

string Drink::GetName() const
{
	return m_name;
}

void Drink::SetName(string name)
{
	m_name = name;
}

ostream& Drink::display(ostream& os) const
{
	os << GetName() << " which is a " << PrintType() << " with a volume of " << GetVolume() << " liters and costs " << GetPrice() << " ron. ID: " << GetID() << endl;
	return os;
}

string Drink::getData() const
{
	string line;
	line = "Drink, " + to_string(GetID()) + ", " + to_string(GetPrice()) + ", " + to_string(GetVolume()) + ", " + to_string(GetType()) + ", " +PrintType() + ", " + GetName();
	// line += "\n";
	return line;
}

ostream& operator<<(ostream& os, const Drink& p)
{
	return p.display(os);
}
