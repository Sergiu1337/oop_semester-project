#include "ProductRepository.h"
#include <algorithm>
#include <fstream>
#include <sstream>
using namespace std;
bool ProductRepository::checkID(int id)
{
	for (auto& it : m_repo) 
		if (it->GetID() == id)	
			return false;
	return true;
}

void ProductRepository::addProduct(Product* p)
{
	m_repo.push_back(p);
}

void ProductRepository::addProduct(Bread* b)
{
	m_repo.push_back(b);
}

void ProductRepository::addProduct(Pastry* p)
{
	m_repo.push_back(p);
}

void ProductRepository::addProduct(Drink* d)
{
	m_repo.push_back(d);
}

Product* ProductRepository::removeProduct(int id)
{
	Product* p = nullptr;
	auto index = find_if(m_repo.begin(), m_repo.end(), [id](Product* p)->bool {
		return p->GetID() == id;
		});
	if (index != m_repo.end()) {
		p = *index;
		m_repo.erase(index);
	}
	return p;
}

ostream& ProductRepository::display(ostream& os, bool(*filterFunction)(Product* p))
{
	for (auto i = 0; i < m_repo.size(); i++)
		if (filterFunction(m_repo[i]) == true)
			os << *m_repo[i]<< endl;
	return os;
}

string ProductRepository::returnString() const
{
	string line;
	for (int i = 0; i < m_repo.size(); i++)
		line += m_repo[i]->getData() + "\n";
	return line;
}

/*
fstream file;
	file.open(filename, ofstream::out | ofstream::trunc);
	string line;
	line += "ProductType,ID,Price(ron),Volume/Weight(Liters/KGs),TypeNumber,TypeName,Name,Multiplier\n";
	for (int i = 0; i < m_repo.size(); i++)
		line += m_repo[i]->getData() + "\n";
	file << line;
	file.close();
*/
