#pragma once
#include "ProductRepository.h"
#include <stack>
using namespace std;

enum class ActionType {
	ADD,
	REMOVE
};
class ProductController
{
private:
	ProductRepository m_repo;
	std::stack<pair<ActionType, Product*>> m_undoStack;
	std::stack<pair<ActionType, Product*>> m_redoStack;
public:
	ProductController(ProductRepository& r);

	bool checkID(int id);

	void addProduct(Product* p);
	void addProduct(Bread* p);
	void addProduct(Pastry* p);
	void addProduct(Drink* p);
	//apalam m_repo.add(a)

	void displayByPrice(double price);
	// apelam displayRepo
	//m_repo.display(cout,filteAct);  -------> define filetrAct

	void removeByID(int productID);
	void diplayAll();

	void readFromFile(string filename);
	void writeToFile(string filename);
};
