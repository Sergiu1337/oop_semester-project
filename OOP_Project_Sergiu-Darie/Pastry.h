#pragma once
#include"Product.h"

typedef enum
{
	Croissant,
	Muffin,
	Cake
}PastryType;


class Pastry : public Product {
public:
	Pastry(int id, double price, double weight, PastryType type, int multiplier);

	double GetPrice() const override;

	double GetWeight() const;
	void SetWeight(double weight);

	PastryType GetType() const;
	string PrintType() const;
	void SetType(PastryType type);

	int GetMultiplier() const;
	void SetMultiplier(int multiplier);

	ostream& display(ostream& os) const;

	friend ostream& operator <<(ostream& os, const Pastry& p);

	string getData() const;
private:
	double m_weight;
	PastryType m_type;
	int m_multiplier;
};
