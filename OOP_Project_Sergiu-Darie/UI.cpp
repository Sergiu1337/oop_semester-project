#include"UI.h"
#include"ClearScreen.h"
#include<iostream>
#include<string>
#include<stdlib.h>
#include"Product.h"
#include"Bread.h";
#include"Pastry.h"
#include"Drink.h"
using namespace std;

UI::UI(ProductController& controller) : m_controller{ controller }
{
}

void UI::chooseProductType()
{
	cout << "Please choose which product you'd like to add: \n" << endl;
	cout << "\t 1 - Basic Product \n";
	cout << "\t 2 - Bread \n";
	cout << "\t 3 - Pastry \n";
	cout << "\t 4 - Drink \n";
	cout << "\n";
	cout << "\t 0 - Cancel \n";
	cout << endl << "Your option is: ";

	char option = 's';
	while (option != '0') {
		cin >> option;
		switch (option) {
		case'1':
			addProduct();
			option = '0';
			break;
		case'2':
			addBread();
			option = '0';
			break;
		case'3':
			addPastry();
			option = '0';
			break;
		case'4':
			addDrink();
			option = '0';
			break;
		case'0':
			break;
		default:
			cout << "!!! Wrong value, try again !!!" << endl;
		}
		cout << endl;
	}
}

void UI::addProduct()
{
	int id;
	bool boolID = false;
	double price;

	cout << endl;
	cout << "Please insert the id: ";
	while (boolID == false) 
	{
		cin >> id;
		boolID = m_controller.checkID(id);
		if (boolID == false)
			cout << "ID already in use. Please type another one: ";
	}
	cout << "Please insert the price: ";
	cin >> price;
	cout << endl;

	Product* P = new Product(id, price);
	m_controller.addProduct(P);
}

void UI::addBread()
{
	string BreadTypes[] = {
	   "Sweet Bread",
	   "Olives Bread",
	   "Garlic Bread",
	   "White Bread",
	   "Dark Bread",
	   "Potato Bread"
	};

	int id;
	bool boolID = false;
	double price;
	double weight;
	char typeOption = 's';
	BreadType type;

	cout << endl;
	cout << "Please insert the id: ";
	while (boolID == false)
	{
		cin >> id;
		boolID = m_controller.checkID(id);
		if (boolID == false)
			cout << "ID already in use. Please type another one: ";
	}
	cout << "Please insert the price: ";
	cin >> price;
	cout << "Please insert the weight in KGs: ";
	cin >> weight;
	cout << "Please insert the type of bread: " << endl;
	for (int i = 0; i < 6; i++) {
		cout << "\t Option " << i + 1 << ": " << BreadTypes[i] << endl;
	}
	while (typeOption == 's') {
		cin >> typeOption;
		typeOption--;
		switch (typeOption) {
		case'0':
			type = Sweet_Bread;
			break;
		case'1':
			type = Olives_Bread;
			break;
		case'2':
			type = Garlic_Bread;
			break;
		case'3':
			type = White_Bread;
			break;
		case'4':
			type = Dark_Bread;
			break;
		case'5':
			type = Potato_Bread;
			break;
		default:
			typeOption = 's';
			cout << "!!! Wrong value, try again !!!" << endl;
			break;
		}
		cout << endl;
	}

	Bread* B = new Bread(id, price, weight, type);
	m_controller.addProduct(B);
}

void UI::addPastry()
{
	string PastryTypes[] = {
	"Croissant",
	"Muffin",
	"Cake"
	};

	int id;
	bool boolID = false;
	double price;
	double weight;
	char typeOption = 's';
	PastryType type;
	int multiplier;

	cout << endl;
	cout << "Please insert the id: ";
	while (boolID == false)
	{
		cin >> id;
		boolID = m_controller.checkID(id);
		if (boolID == false)
			cout << "ID already in use. Please type another one: ";
	}
	cout << "Please insert the price: ";
	cin >> price;
	cout << "Please insert the weight in KGs: ";
	cin >> weight;
	cout << "Please insert the type of pastry: " << endl;
	for (int i = 0; i < 3; i++) {
		cout << "\t Option " << i + 1 << ": " << PastryTypes[i] << endl;
	}
	while (typeOption == 's') {
		cin >> typeOption;
		typeOption--;
		switch (typeOption) {
		case'0':
			type = Croissant;
			break;
		case'1':
			type = Muffin;
			break;
		case'2':
			type = Cake;
			break;
		default:
			typeOption = 's';
			cout << "!!! Wrong value, try again !!!" << endl;
			break;
		}
		cout << endl;
	}
	cout << "Please insert the multiplier: ";
	cin >> multiplier;

	Pastry* P = new Pastry(id, price, weight, type, multiplier);
	m_controller.addProduct(P);
}

void UI::addDrink()
{
	string DrinkTypes[] = {
	   "Fuzzy Drink",
	   "Calm Drink",
	   "Natural Water",
	   "Carbonated Water"
	};
	int id;
	bool boolID = false;
	double price;
	double volume;
	char typeOption = 's';
	DrinkType type;
	string name;

	cout << endl;
	cout << "Please insert the id: ";
	while (boolID == false)
	{
		cin >> id;
		boolID = m_controller.checkID(id);
		if (boolID == false)
			cout << "ID already in use. Please type another one: ";
	}
	cout << "Please insert the price: ";
	cin >> price;
	cout << "Please insert the volume in Liters: ";
	cin >> volume;
	cout << "Please insert the type of drink: " << endl;
	for (int i = 0; i < 4; i++) {
		cout << "\t Option " << i + 1 << ": " << DrinkTypes[i] << endl;
	}
	while (typeOption == 's') {
		cin >> typeOption;
		typeOption--;
		switch (typeOption) {
		case'0':
			type = Fuzzy_Drink;
			break;
		case'1':
			type = Calm_Drink;
			break;
		case'2':
			type = Natural_Water;
			break;
		case'3':
			type = Carbonated_Water;
			break;
		default:
			typeOption = 's';
			cout << "!!! Wrong value, try again !!!" << endl;
			break;
		}
		cout << endl;
	}
	cout << "Please insert the name: ";
	cin >> name;

	Drink* D = new Drink(id, price, volume, type, name);
	m_controller.addProduct(D);
}

void UI::removeProduct()
{
	int ID;
	cout << "Please enter the id you want to remove: ";
	cin >> ID;
	m_controller.removeByID(ID);
}

void UI::show()
{
	char option;
	do {
		cout << "Please insert your option: \n" << endl;
		cout << "\t 1 - AddProduct \n";
		cout << "\t 2 - RemoveProduct \n";
		cout << "\t 3 - DisplayProducts \n";
		cout << "\t 4 - Write to file \n";
		cout << "\t 5 - Read from file \n";
		cout << "\n";
		cout << "\t 0 - ExitProgram \n";
		cout << endl << "Your option is: ";
		cin >> option;
		ClearScreen();
		switch (option)
		{
		case'1':
			chooseProductType();
			ClearScreen();
			break;
		case'2':
			removeProduct();
			ClearScreen();
			break;
		case'3':
			m_controller.diplayAll();
			break;
		case'4':
			ClearScreen();
			m_controller.writeToFile("newfile.csv");
			break;
		case'5':
			ClearScreen();
			m_controller.readFromFile("newfile.csv");
			break;
		case'0':
			exit(0);
			break;
		default:
			ClearScreen();
			cout << "!!! No such option exists !!!\n";
			break;
		}
	} while (true);

}
