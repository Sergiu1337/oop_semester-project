#pragma once
#include"Product.h"

typedef enum
{
	Sweet_Bread,
	Olives_Bread,
	Garlic_Bread,
	White_Bread,
	Dark_Bread,
	Potato_Bread
}BreadType;


class Bread : public Product {
public:
	Bread(int id, double price, double weight, BreadType type);

	double GetPrice() const override;

	double GetWeight() const;
	void SetWeight(double weight);

	BreadType GetType() const;
	string PrintType() const;
	void SetType(BreadType type);

	ostream& display(ostream& os) const;

	friend ostream& operator <<(ostream& os, const Bread& p);

	string getData() const;
private:
	double m_weight;
	BreadType m_type;
};
