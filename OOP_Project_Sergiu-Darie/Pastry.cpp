#include "Pastry.h"

string PastryTypes[] = {
	"Croissant",
	"Muffin",
	"Cake"
};

Pastry::Pastry(int id, double price, double weight, PastryType type, int multiplier) : Product(id, price)
{
	m_weight = weight;
	m_type = type;
	m_multiplier = multiplier;
}

double Pastry::GetPrice() const
{
	double price = Product::GetPrice() * m_multiplier * m_weight;
	return price;
}

double Pastry::GetWeight() const
{
	return m_weight;
}

void Pastry::SetWeight(double weight)
{
	m_weight = weight;
}

PastryType Pastry::GetType() const
{
	return m_type;
}

string Pastry::PrintType() const
{
	return PastryTypes[GetType()];
}

void Pastry::SetType(PastryType type)
{
	m_type = type;
}

int Pastry::GetMultiplier() const
{
	return m_multiplier;
}

void Pastry::SetMultiplier(int multiplier)
{
	m_multiplier = multiplier;
}

ostream& Pastry::display(ostream& os) const
{
	os << PrintType() << " which costs: " << GetPrice() << " ron and weights: " << GetWeight() << " kgs. ID: " << GetID() << endl;
	return os;
}

string Pastry::getData() const
{
	string line;
	line = "Pastry, " + to_string(GetID()) + ", " + to_string(GetPrice()) + ", " + to_string(GetWeight()) + ", " +  to_string(GetType()) + ", , , " + to_string(GetMultiplier());
	// line += "\n";
	return line;
}

ostream& operator<<(ostream& os, const Pastry& p)
{
	return p.display(os);
}
