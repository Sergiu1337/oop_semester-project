#pragma once
#include "ProductController.h"
class UI
{

private:
	ProductController m_controller;

public:

	UI(ProductController& controller);

	void chooseProductType();
	void addProduct();
	void addBread();
	void addPastry();
	void addDrink();
	void removeProduct();
	void show();
};
