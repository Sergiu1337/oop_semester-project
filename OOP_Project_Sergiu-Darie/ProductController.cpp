#include "ProductController.h"
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

ProductController::ProductController(ProductRepository& r)
{
	m_repo = r;
}

bool ProductController::checkID(int id)
{
	return m_repo.checkID(id);
}

void ProductController::addProduct(Product* p)
{
	m_repo.addProduct(p);
	m_undoStack.push(make_pair(ActionType::ADD, p));
}

void ProductController::addProduct(Bread* p)
{
	m_repo.addProduct(p);
	m_undoStack.push(make_pair(ActionType::ADD, p));
}

void ProductController::addProduct(Pastry* p)
{
	m_repo.addProduct(p);
	m_undoStack.push(make_pair(ActionType::ADD, p));
}

void ProductController::addProduct(Drink* p)
{
	m_repo.addProduct(p);
	m_undoStack.push(make_pair(ActionType::ADD, p));
}

void ProductController::removeByID(int productID)
{
	Product* p = m_repo.removeProduct(productID);
	if (p) m_undoStack.push(make_pair(ActionType::REMOVE, p));
}

void ProductController::diplayAll()
{
	m_repo.display(cout, [](Product* p)->bool {return true; });
}

void ProductController::readFromFile(string filename)
{
	// WORK IN PROGRESS
	fstream file;
	file.open(filename);
	vector<string> row;
	string line, word, temp;
	getline(file, line);	// this is the main line which writes the labels
	while (file >> temp) {
	row.clear();
	getline(file, line);  // read an entire row and store it in a string variable 'line'
	stringstream s(line); // used for breaking words

	// read every column data of a row and
	// store it in a string variable, 'word'
	while (getline(s, word, ',')) {
		// add all the column data
		// of a row to a vector
		row.push_back(word);
	}
	cout << row[0] << endl;
	}
}

void ProductController::writeToFile(string filename)
{
	fstream file;
	string line = "ProductType, ID, Price(ron), Volume/Weight(Liters/KGs), TypeNumber, TypeName, Name, Multiplier\n";
	line += m_repo.returnString();
	file.open(filename, ofstream::out | ofstream::trunc);
	file << line;
	file.close();
}
